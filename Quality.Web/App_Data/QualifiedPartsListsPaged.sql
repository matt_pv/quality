USE [QUALITY]
GO

/****** Object:  StoredProcedure [dbo].[QualifiedPartsListsPaged]    Script Date: 9/30/2017 12:09:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[QualifiedPartsListsPaged]
  @PageNumber INT = 1,
  @PageSize   INT = 5
AS
BEGIN
  SET NOCOUNT ON;

select p.PartNumber,q.Revision as PartRevision,p.PartName,q.ToolDieSetNumber,q.IsQualified as Qpl,q.OpenPo,
p.Jurisdication as PartJurisdiction,p.Classification as PartClassification,c.CompanyName as SupplierCompanyName,sc.SupplierCodes as SupplierCompanyCode,q.Ctq,u.UserName as 'QplLastUpdatedBy',q.LastUpdatedDateUtc as 'QplLastUpdatedDate',q.ExpirationDateUtc as QplExipreDate
from QualifiedPartsLists q 
	inner join Parts p on q.PartId=p.PartId
	left join Companies c on c.Id=q.SupplierCompanyId
	inner join CustomerSupplierXref sc on sc.SupplierCompanyId=q.SupplierCompanyId
	left join Users u on u.Id=q.LastUpdatedById

ORDER BY  q.PartId
  OFFSET @PageSize * (@PageNumber - 1) ROWS
  FETCH NEXT @PageSize ROWS ONLY

  END

GO


