﻿
using System;
using System.Threading.Tasks;
using System.Web.Http;
using Quality.Dal;

namespace Quality.Web.Controllers
{

    public class QualifiedPartsController : ApiController
    {
        private readonly QplDataService _qplDataService;

        public QualifiedPartsController(QplDataService qplDataService)
        {
            _qplDataService = qplDataService;
        }

        #region Qualified Parts APIs

        [ActionName("GetQplData")]
        [HttpGet]
        public async Task<IHttpActionResult> GetQplData(int pagenumber, int pagesize)
        {
            try
            {
                
                var data = _qplDataService.GetQplPagedData(pagenumber, pagesize);                
                return Ok(data);

            }
            catch (Exception exception)
            {
                //log exception to a lof store here
                return InternalServerError(exception);
            }
        }

        #endregion

        

    }
}