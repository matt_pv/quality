var app;
(function (app) {
    var Common;
    (function (Common) {
        var DataAccessService = (function () {
            function DataAccessService(httpService, window) {
                this.httpService = httpService;
                this.window = window;
                this.storage = this.window.sessionStorage;
            }
            DataAccessService.prototype.getJsonData = function (url) {
                //config structure
                var config = {};
                //add config and just return the promise directly instead of creating a deferred object. Promises are chainable
                return this.httpService.get(url, config)
                    .then(function (result) { return result.data; });
            };
            return DataAccessService;
        }());
        DataAccessService.$inject = ['$http', '$window'];
        Common.DataAccessService = DataAccessService;
        angular
            .module("common.services", [])
            .service("dataAccessService", DataAccessService);
    })(Common = app.Common || (app.Common = {}));
})(app || (app = {}));
//# sourceMappingURL=dataAccessServices.js.map