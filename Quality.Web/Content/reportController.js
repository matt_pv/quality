var app;
(function (app) {
    var QuoteController;
    (function (QuoteController) {
        var ReportController = (function () {
            function ReportController(dataAccessService) {
                this.dataAccessService = dataAccessService;
                this.page = 1;
                this.pagesize = "10";
                this.pageoptions = [
                    { label: '10', value: "10" },
                    { label: '20', value: "20" },
                    { label: '50', value: "50" }
                ];
                var self = this;
                self.title = "View QPL List";
                self.getQplData();
            }
            ReportController.prototype.getQplData = function () {
                var self = this;
                var qpldata = self.dataAccessService.getJsonData("./api/QualifiedParts/GetQplData?pagenumber=" + self.page + "&pagesize=" + self.pagesize);
                qpldata.then(function (data) {
                    self.qpldata = data.DataList;
                    self.totalrow = data.Total;
                    self.totalpage = Math.ceil(data.Total / parseInt(self.pagesize));
                    console.log(data);
                });
            };
            ReportController.prototype.fristPage = function () {
                var self = this;
                self.page = 1;
                self.getQplData();
            };
            ReportController.prototype.previousPage = function () {
                var self = this;
                if (self.page > 1)
                    self.page--;
                self.getQplData();
            };
            ReportController.prototype.nextPage = function () {
                var self = this;
                if (self.page < self.totalpage)
                    self.page++;
                self.getQplData();
            };
            ReportController.prototype.lastPage = function () {
                var self = this;
                self.page = self.totalpage;
                self.getQplData();
            };
            return ReportController;
        }());
        ReportController.$inject = ['dataAccessService'];
        QuoteController.ReportController = ReportController;
        angular
            .module("Report")
            .controller("ReportController", ReportController);
    })(QuoteController = app.QuoteController || (app.QuoteController = {}));
})(app || (app = {}));
//# sourceMappingURL=reportController.js.map