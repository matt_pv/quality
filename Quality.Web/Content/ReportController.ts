﻿module app.QuoteController {

    export class ReportController {
        title: string;
        page: number = 1;
        pagesize:string="10";
        storage: any;
        qpldata: any;
        totalrow: number;
        totalpage: number;
        displayedCollection: any;
        pageoptions = [
            { label: '10', value: "10" },
            { label: '20', value: "20" },
            { label: '50', value: "50" }
        ];
       
        static $inject = ['dataAccessService'];
        constructor(private dataAccessService: app.Common.DataAccessService) {
            var self = this;
            self.title = "View QPL List";             
            self.getQplData();
        }
        getQplData() {
            var self = this;
            var qpldata = self.dataAccessService.getJsonData(`./api/QualifiedParts/GetQplData?pagenumber=${self.page}&pagesize=${self.pagesize}`);
            qpldata.then((data:any) => {
                self.qpldata = data.DataList;
                self.totalrow = data.Total;
                self.totalpage = Math.ceil(data.Total / parseInt(self.pagesize));
                console.log(data);
            });
        }
        fristPage() {
            var self = this;
            self.page = 1;
            self.getQplData();
        }
        previousPage() {
            var self = this;
            if (self.page > 1)
                self.page--;

            self.getQplData();
        }
        nextPage() {
            var self = this;
            if (self.page < self.totalpage)
                self.page++;

            self.getQplData();
        }
        lastPage() {
            var self = this;
            self.page = self.totalpage;
            self.getQplData();
        }

    }

    angular
        .module("Report")
        .controller("ReportController",
            ReportController);
}