﻿module app.Common {
    interface IDataAccessService {
        getJsonData(url: string): angular.IPromise<Object[]>;
    }
    export class DataAccessService implements IDataAccessService {

        storage: any;
        constructor(private httpService: angular.IHttpService, private window: ng.IWindowService) {
            this.storage = this.window.sessionStorage;
        }


        static $inject = ['$http', '$window'];

        getJsonData(url: string): angular.IPromise<Object[]> {

            //config structure
            var config: angular.IRequestShortcutConfig = {
            }
            //add config and just return the promise directly instead of creating a deferred object. Promises are chainable
            return this.httpService.get(url, config)
                .then((result: any) => result.data);
        }
      
    }

    angular
        .module("common.services", [])
        .service("dataAccessService", DataAccessService);
}
