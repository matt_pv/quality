using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Quality.Models;

namespace Quality.Dal
{
    public class QplDataService
    {
        private readonly string _connectionString;
        public QplDataService()
        {
            _connectionString= ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        public int GetQplPagedDataTotal()
        {         
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("GetQualifiedPartsListTotal", conn) { CommandType = CommandType.StoredProcedure };
                using (var rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        return Convert.ToInt32(rdr["total"].ToString());
                    }
                }
                return 0;
            }
        }

        public QplPaged GetQplPagedData(int pagenumber,int pagesize)
        {
            var data = new QplPaged
            {
                Total = GetQplPagedDataTotal(),
                DataList = new List<QplViewModel>()
            };
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("QualifiedPartsListsPaged", conn) {CommandType = CommandType.StoredProcedure};

                cmd.Parameters.Add(new SqlParameter("@PageNumber", pagenumber));
                cmd.Parameters.Add(new SqlParameter("@PageSize", pagesize));                
                using (var rdr = cmd.ExecuteReader())
                {                    
                    while (rdr.Read())
                    {
                        var qplrow = new QplViewModel
                        {
                            PartNumber = rdr["PartNumber"].ToString(),
                            PartRevision = rdr["PartRevision"] == DBNull.Value ? "" : rdr["PartRevision"].ToString(),
                            PartName = rdr["PartName"].ToString(),
                            ToolDieSetNumber = rdr["ToolDieSetNumber"] == DBNull.Value
                                ? ""
                                : rdr["ToolDieSetNumber"].ToString(),
                            Qpl = Convert.ToBoolean(rdr["Qpl"]),
                            OpenPo = Convert.ToBoolean(rdr["OpenPo"]),
                            PartJurisdiction = rdr["PartJurisdiction"].ToString(),
                            PartClassification = rdr["PartClassification"].ToString(),
                            SupplierCompanyName = rdr["SupplierCompanyName"].ToString(),
                            SupplierCompanyCode = rdr["SupplierCompanyCode"].ToString(),
                            Ctq = Convert.ToBoolean(rdr["Ctq"]),
                            QplLastUpdatedBy = rdr["QplLastUpdatedBy"].ToString(),
                            QplLastUpdatedDate = Convert.ToDateTime(rdr["QplLastUpdatedDate"].ToString()),                            
                        };
                        //there is a runtime error if you use ternary operator in the above code block.
                        if (rdr["QplExipreDate"] != DBNull.Value)
                            qplrow.QplExipreDate = Convert.ToDateTime(rdr["QplExipreDate"].ToString());

                        data.DataList.Add(qplrow);
                    }
                }
                return data;
            }
        }
    }
}