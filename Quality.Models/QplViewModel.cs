﻿using System;
using System.Collections.Generic;

namespace Quality.Models
{
    public class QplViewModel
    {
        public string PartNumber { get; set; }
        public string PartRevision { get; set; }
        public string PartName { get; set; }
        public string ToolDieSetNumber { get; set; }
        public bool Qpl { get; set; }
        public bool OpenPo { get; set; }
        public string PartJurisdiction { get; set; }
        public string PartClassification { get; set; }
        public string SupplierCompanyName { get; set; }
        public string SupplierCompanyCode { get; set; }
        public bool Ctq { get; set; }
        public string QplLastUpdatedBy { get; set; }
        public DateTime QplLastUpdatedDate { get; set; }
        public DateTime? QplExipreDate { get; set; }
    }

    public class QplPaged
    {
        public List<QplViewModel> DataList { get; set; }
        public  int Total { get; set; }
    }
}